from flask import Flask, request, render_template, make_response
import requests
import datetime
import os
import json
from flask_cors import CORS
from DatabaseHandler import DatabaseHandler
from facedetection import *
from base64encoding import *


mysql_ip = os.getenv('MYSQL_IP')

URL = "http://localhost:3000/api/chat"

b_handler = DatabaseHandler(
    host="db", port='3306', user='root', password='root123', database='chat_db')


def get_messages_history(username):
    user_history = db_handler.retrieveMessage(username=username)
    user_history = [('o sobie', 'This is a response to the message'),('o tobie', 'This is a response to the message2')]
    return serialize_user_history(user_history)


def serialize_user_history(user_history):
    serialized_history = []

    for message_pair in user_history:
        # Dodanie wiadomości użytkownika do historii
        serialized_history.append({
            "role": "user",
            "content": message_pair[0]
        })

        # Dodanie odpowiedzi asystenta do historii
        serialized_history.append({
            "role": "assistant",
            "content": message_pair[1]
        })

    return serialized_history


def new_question(question):
    return {
        "role": "user",
        "content": question
    }


def get_json_from_scheme(question):
    messages_history = get_messages_history()
    messages_history.append(new_question(question))

    json_data = {
        "model": {
            "id": "/models/llama-2-70b-chat.bin",
            "name": "Llama 2 70B",
            "maxLength": 12000,
            "tokenLimit": 4000
        },
        "messages": messages_history,
        "key": "",
        "prompt": "You are a helpful and friendly AI assistant. Never lie to me",
        "temperature": 1
    }

    return json.dumps(json_data)


def send_req(question):
    json_data = get_json_from_scheme(question)
    HEADERS = {
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Accept': '*/*'
    }
    # print(json_data)
    response = requests.post(URL, json=json.loads(json_data), headers=HEADERS)
    # print(response.text)
    return response


frontend_dir = os.path.abspath('./projekt-grupowy-ChatGPT-klient/chatUI')
app = Flask(__name__, template_folder=frontend_dir,
            static_folder=frontend_dir)
CORS(app)


@app.route('/<string:page_name>/')
def render_static(page_name):
    return render_template(f"/{page_name}.html")


@app.route('/message', methods=['POST'])
def procces_request():

    userTranslated = request.form['username']
    messageTranslated = request.form['message']
    # print(request.values)

    response_from_model = "This is a response to the message"

    # tutaj mozesz wywolac swoja funkcję dla message_data, username_data, response_from_model
    db_handler.addMessage(username=userTranslated,
                          message=messageTranslated, response=response_from_model)

    # response_from_model = send_req("Tell me a joke")
    response_to_this_post = make_response("here will be response ", 200)
    response_to_this_post.headers["Content-Type"] = "text/plain"
    return response_to_this_post


@app.route('/photo', methods=['POST'])
def photo_request():
    image_data = request.form['imageData']
    imageTranslated = request.form['imageData']
    decoded_image = decodeImage(image_data)
    writeImageFileData("temp\img.jpg", image_data)
    username = compareFaces("VGG-Face", "temp\img.jpg", "photos")

    if os.path.exists("photos/representations_vgg_face.pkl"):
        os.remove("photos/representations_vgg_face.pkl")

    if username != "":
        response_message = username
        status_code = 200
        response_to_this_post = make_response(response_message, status_code)
        # wywolanie funkcji zapisania zdjecia do bazy danych do sciezki /username

    else:
        response_message = "not found"
        status_code = 404
        # jezeli nie znaleziony na razie nic nie zapisujemy, po stronie klienta bedzie ponowne przeslanie zdjecia wraz z imieniem
        response_to_this_post = make_response(response_message, status_code)
    return response_to_this_post


@app.route('/new-photo', methods=['POST'])
def new_photo_request():
    image_data = request.form['imageData']
    username_data = request.form['usernameData']

    db_handler.createUser(username=username_data)
    db_handler.addPhoto(username=username_data, path_to_photo=image_data)
    # zapisanie zdjecia i nazwy uzytkownika zarowno w folderze jak i bazie danych

    response_to_this_post = make_response("Udalo sie", 200)
    return response_to_this_post


@app.route('/rate', methods=['POST'])
def rate_request():
    rateTranslated = request.form['rate']
    userTranslated = request.form['username']

    db_handler.giveRating(username=userTranslated, rating=rateTranslated)
    response_to_this_post = make_response("Udalo sie", 200)
    return response_to_this_post


@app.route('/chat-history', methods=['POST'])
def get_chat_history():
    userTranslated = request.form['username']
    user_history = db_handler.retrieveMessageWithDate(username=userTranslated)
    # user_history = [('o sobie', 'This is a response to the message', datetime.datetime(2024, 1, 28, 14, 44, 14)),('o tobie', 'This is a response to the message2', 'datetime.datetime(2024, 1, 28, 14, 44, 14)')]
    # user_history = [('o sobie', 'This is a response to the message', datetime.datetime(2024, 1, 28, 14, 44, 14)),('o tobie', 'This is a response to the message2', datetime.datetime(2024, 1, 28, 14, 44, 14))]
    user_history = [convert_date_to_string(entry) for entry in user_history]
    # print(user_history)
    response_to_this_post = make_response(user_history, 200)
    return response_to_this_post


def convert_date_to_string(history_entry):
    return tuple(str(value) if isinstance(value, datetime.datetime) else value for value in history_entry)


@app.route('/audio', methods=['POST'])
def audio_request():
    audio_data = request.get_data('audio')

    # uruchomienie funkcji przetwarzajacej format blob na mp3
    # zapisanie .mp3 jako plik do folderu z nagraniami
    # funckcja przeksztalcajaca nagranie na tekst przekazujac sciezke do nagrania
    # przekazanie odczytanego tekstu jako zapytanie do modelu
    # response_from_model = send_req("Tell me a joke")

    # zwrocenie odpowiedzi na zapytanie
    response_to_this_post = make_response(
        "To jest odpowiedz na pytanie audio", 200)
    return response_to_this_post


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
