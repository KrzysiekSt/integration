import mysql.connector
from datetime import datetime


class DatabaseHandler:
    def __init__(self, host, port, user, password, database):
        try:
            self.conn = mysql.connector.connect(
                host=host, port=port, user=user, password=password, database=database)
            self.cursor = self.conn.cursor()
            # You can use it to test out the connection
            try:
                cursor = self.conn.cursor()
                cursor.execute("SELECT VERSION()")

                result = cursor.fetchone()
                print("MySQL Server Version:", result[0])

            except mysql.connector.Error as err:
                print("Error: {}".format(err))
        except mysql.connector.Error as err:
            print(f"Couldn't connect to to a database: {err}")
            raise

    def createUser(self, username):
        try:
            insert_query = "INSERT INTO `User` (`username`) VALUES (%s);"
            self.cursor.execute(insert_query, (username,))
            self.conn.commit()
            print("User added")
        except mysql.connector.Error as err:
            print(f"Error creating user: {err}")

    def getUserIdByUsername(self, username):
        try:
            selectID_query = "SELECT `id_user` FROM `User` WHERE `username` = %s;"
            self.cursor.execute(selectID_query, (username,))
            result = self.cursor.fetchone()
            if result:
                return result[0]
            else:
                return None
        except mysql.connector.Error as err:
            print(f"Error getting user ID: {err}")
            return None

    def addPhoto(self, username, path_to_photo):
        try:
            insert_photo_query = "INSERT INTO `Photo` (`date`, `user`, `path_to_photo`) VALUES (%s, %s, %s);"
            user_id = self.getUserIdByUsername(username)

            if user_id is not None:
                current_datetime = datetime.now()
                self.cursor.execute(insert_photo_query,
                                    (current_datetime, user_id, path_to_photo))
                self.conn.commit()
                print("Photo added")
            else:
                print("User not found.")
        except mysql.connector.Error as err:
            print(f"Error adding photo: {err}")

    def addMessage(self, username, message, response):
        try:
            insert_message_query = "INSERT INTO `Messages` (`user`, `message`, `response`, `date`) VALUES (%s, %s, %s, %s);"
            user_id = self.getUserIdByUsername(username)

            if user_id is not None:
                current_datetime = datetime.now()
                self.cursor.execute(
                    insert_message_query, (user_id, message, response, current_datetime))
                self.conn.commit()
                print("Message added")
            else:
                print("User not found.")
        except mysql.connector.Error as err:
            print(f"Error adding message: {err}")

    def retrieveMessage(self, username):
        try:
            retrieve_message_query = "SELECT `message`, `response` FROM `Messages` WHERE `user` = %s;"
            user_id = self.getUserIdByUsername(username)

            if user_id is not None:
                self.cursor.execute(retrieve_message_query, (user_id,))
                result = self.cursor.fetchall()
                return result
            else:
                print("User not found.")
        except mysql.connector.Error as err:
            print(f"Error retrieving message: {err}")
            return None

    def giveRating(self, username, rating):
        try:
            give_rating_query = "INSERT INTO `Rating` (`user`, `rating`) VALUES (%s, %s);"
            user_id = self.getUserIdByUsername(username)
            if user_id is not None:
                self.cursor.execute(give_rating_query, (user_id, rating))
                self.conn.commit()
                print("Rating added")
            else:
                print("User not found.")
        except mysql.connector.Error as err:
            print(f"Error adding rating: {err}")
            return None

    def close_connection(self):
        try:
            self.conn.close()
        except mysql.connector.Error as err:
            print(f"Error closing connection: {err}")

    def retrieveMessageWithDate(self, username):
        try:
            retrieve_message_query = "SELECT message, response, date FROM Messages WHERE user = %s;"
            user_id = self.getUserIdByUsername(username)

            if user_id is not None:
                self.cursor.execute(retrieve_message_query, (user_id,))
                result = self.cursor.fetchall()
                return result
            else:
                print("User not found.")
        except mysql.connector.Error as err:
            print(f"Error retrieving message: {err}")
            return None

# Example on how to call those methods somewhere else
# from DatabaseHandler import DatabaseHandler
# db_handler = DatabaseHandler(host='127.0.0.1', port='3306', user='root', password='password', database='lamachat')
# db_handler.create_user('example_user')
# db_handler.add_photo('example_user', 'example_path_to_photo')
# db_handler.add_message('example_user', 'example_message', 'example_response')
# db_handler.retrieveMessage('example_user')
